#!/usr/bin/env bash
#
# MOUNTLOCK
# The intended use of this bash script is to wrap a process while creating a
# lock file on a mounted path to prevent the same process from executing
# concurrently on multiple docker instances.
#
# Turns out that while there are a lot of ways to accomplish this on local file
# systems (such as flock or mkdir) these methods are not dependable over nfs
# mounts. To make matters worse there are but few atomic operations over nfs.
# The favored method, which is employed here, is hard links.
#
# Targeting Bash 4+
#
# Also see:
# http://0pointer.de/blog/projects/locking.html
# https://www.experts-exchange.com/questions/10078625
# http://man7.org/linux/man-pages/man2/open.2.html (section O_EXCL)
#
# 2023-03-30
# jphaskins@gmail.com
#

# path to the mount directory that will hold the lockfiles
LOCK_DIR="/vol/mountlock"

# name of a file that exists to show the mount is active
MOUNT_SANITY_FILE=".mounted_do_not_delete"

# maximum time in seconds we should honor a lock (two days by default)
STALE_TIMEOUT="172800"

LOG_LEVEL="error"

function main {

    local id

    # parse arguments
    while true; do
        case "${1}" in

            "--lock-dir")
                LOCK_DIR="${2}"
                shift 2;;

            "--sanity-file")
                MOUNT_SANITY_FILE="${2}"
                shift 2;;

            "--stale-timeout")
                STALE_TIMEOUT="${2}"
                shift 2;;

            "--log-level")
                LOG_LEVEL="${2}"
                shift 2;;

            "--id")
                id="${2}"
                shift 2;;

            "--") # explicitly signal end of options
                shift
                break;;

            *) # default, if not an option then break out of the loop
                break;;
        esac
    done

    # don't run anything unless the mount can be verified
    if [ ! -e "${LOCK_DIR}/${mount_sanity_file}" ]; then
        error "Mount verification file missing: ${LOCK_DIR}/${mount_sanity_file}"
    fi

    local command=("$@")

    # first two args as human readable prefix
    local prefix
    printf -v prefix "%s_" "${command[@]:0:2}"

    # strip symbols
    prefix="${prefix//[^a-zA-Z0-9]/_}"

    if [ -z "$id" ]; then
        # hash our command and args (using array assignment to grab first field)
        id=($(echo "${command[@]}" | md5sum))
    fi

    LOCK_FILE="${LOCK_DIR}/${prefix}${id}.lock"

    # by default we want a failed exit status
    LOCK_ACQUIRED=1

    # make sure we release our lock when the script exits for any reason
    trap exit_trap EXIT

    # attempt to lock
    if lock_acquire; then
        log "info" "mountlock acquired"

        # run command
        ${command[@]}
        exit $?

    else
        log "warn" "Unable to acquire mountlock"
    fi
}

function lock_acquire {
    # acquire a lock while invalidating any existing lock that has timed out

    lock_try
    local return_status=$?
    if [ $return_status -ne 0 ]; then
        if lock_is_stale "${LOCK_FILE}"; then
            log "debug" "Stale lock detected, invalidating current lock"
            unlink "${LOCK_FILE}"

            lock_try
            return $?
        fi
    fi

    return $return_status
}

function lock_try {
    # this will identify our attempt to create a lock file, acting as a kind of
    # transaction id. as long as this doesn't collide we are good
    local rand="$(rand 16)"

    # calculate the max time we can hold a lock
    local current_timestamp="$(date +%s)"
    local timeout=$(expr $current_timestamp + $STALE_TIMEOUT)

    # make sure we "close the transaction" on failed attempts
    trap return_trap RETURN

    # create our temp file
    TEMP_FILE="${LOCK_FILE}.${rand}"
    echo "$timeout" > "$TEMP_FILE"

    # attempt to lock. this is usually an atomic operation
    if ln "$TEMP_FILE" "$LOCK_FILE" 2>&-; then
        LOCK_ACQUIRED=0
    else
        # the server might fail at the transport level to return the
        # state of the hard link
        sleep .2
        if lock_is_owned; then
            # the lock must have succeeded despite the reported failure
            LOCK_ACQUIRED=0
        fi
    fi

    return $LOCK_ACQUIRED
}

function lock_release {
    if lock_is_owned; then
        unlink "$LOCK_FILE"
        return $?
    fi

    return 1
}

function lock_is_owned {
    # if our temporary file has a hard link then we own the lock
    local link_num=$(stat "$TEMP_FILE" -c "%h")
    if [ $link_num -eq 2 ]; then
        return 0
    fi

    return 1
}

function lock_is_stale {
    local timeout="$(<"${LOCK_FILE}")"
    local current_timestamp="$(date +%s)"

    if [ $current_timestamp -gt $timeout ]; then
        return 0
    fi

    return 1
}

function rand {
    local len=$1
    echo "$(head /dev/urandom | tr -cd 'a-z0-9' | head -c "$len")"
}

function exit_trap {
    #  clean up only if we hold an active lock
    if [ $LOCK_ACQUIRED -ne 0 ]; then
        return
    fi

    lock_release
    local return_status=$?

    #  should always be the last action we take because
    #  after this we can't track the status of the lock
    unlink "$TEMP_FILE"

    if [ $return_status -eq 0 ]; then
        log "info" "mountlock released"
    else
        error "mountlock failed to release lock"
    fi
}

function return_trap {
    # this will clean up failed lock attempts.

    # for some reason this will trigger for each function on the call stack.
    # we are only interested in the first
    local calling_function="${FUNCNAME[1]}"
    if [ "$calling_function" != "lock_try" ]; then
        return
    fi

    if [ $LOCK_ACQUIRED -ne 0 ]; then
        # lock attempt failed
        unlink "$TEMP_FILE"
    fi
}

function error {
    log "error" "$@"
    exit 1
}

function log {
    local message_level="$1"
    local message="${@:2}"
    local timestamp="$(date +"%Y-%m-%d %H:%M:%S")"
    local numeric_level="$(log_level_translate "$message_level")"
    local numeric_log_level="$(log_level_translate "$LOG_LEVEL")"

    # ouput to stderr if greater than or equal to the current log level
    if [ $numeric_level -ge $numeric_log_level ]; then
        printf "%s\n" "${timestamp} [${message_level^^}] ${message}" 1>&2
    fi
}

function log_level_translate {
    level="$1"

    local debug=10
    local info=20
    local warn=30
    local error=40

    # strip numerals, if any characters remain treat it as a key
    if [ -n "${level//[0-9]}" ]; then
        level="${!level}"
    fi
}

main "$@"
