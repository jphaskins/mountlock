# MOUNTLOCK

The intended use of this bash script is to wrap a process while creating a
lock file on a mounted path to prevent the same process from executing
concurrently on multiple docker instances.

Turns out that while there are a lot of ways to accomplish this on local file
systems (such as flock or mkdir) these methods are not dependable over nfs
mounts. To make matters worse there are but few atomic operations over nfs.
The favored method, which is employed here, is hard links.

Targeting Bash 4+


## Also see:

http://0pointer.de/blog/projects/locking.html

https://www.experts-exchange.com/questions/10078625

http://man7.org/linux/man-pages/man2/open.2.html (section O_EXCL)
